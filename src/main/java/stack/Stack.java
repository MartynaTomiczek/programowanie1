package stack;

public class Stack {
    private int array[];
    private int top;
    private int capacity;

    Stack stack = new Stack(10);

    public Stack (int capacity){
        this.capacity=capacity;
        this.array = new int [capacity];
        this.top=-1;
    }
    public void push (int element) {
        if (isFull()){
            System.out.println("Stack is full");
    } else {
        array[++top]=element;
        }
    }

    private boolean isFull() {
        return top == capacity -1;
    }

    public int pop(){
        return array[top--];
    }
    public int peek(){
        return array[top];
    }
}
