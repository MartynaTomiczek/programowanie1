package chess;

import java.util.Scanner;

public class Chess {

    public static final String WHITE_FIELD=" \u25A0 ";
    public static final String BLACK_FIELD=" \u25A1 ";

    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Podaj dlugosc szachownicy");

            int chess_lenght = scanner.nextInt();
            System.out.println("Rysujesz szachownice o dlugosci" + chess_lenght);

            for (int y=0; y < chess_lenght; y++) {  // pierwsza petla, pierwsza zmnienna
              for (int x = 0; x < chess_lenght; x++) {  // petla w petli, druga zmienna, rysujemy zmienne x
                if ((x+y) % 2 == 0) {
                    System.out.print(WHITE_FIELD);  // unikody
                } else {
                    System.out.print(BLACK_FIELD);
                }
            }

              System.out.println(""); // przechodzimy do nowej lini

            }
        }
}
