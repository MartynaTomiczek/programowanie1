package anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {

        // ROMANTYK -> MARKOTNY
        // sortowanie - posortuj i sprawdź czy są równe
        // AKMNORTY -> AKMNORTY

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwa słowa do sprawdzenia czy są to anagramy");

        String firstWord = scanner.nextLine ();
        System.out.println("Pierwsze słowo: " + firstWord);

        String secondWord = scanner.nextLine();
        System.out.println("Drugie słowo: " + secondWord);

        char[] firstWordCharacters = firstWord.toCharArray();
        char[] secondWordCharacters = secondWord.toCharArray();
        Arrays.sort(firstWordCharacters);
        Arrays.sort(secondWordCharacters);
        boolean areEqual = Arrays.equals(firstWordCharacters, secondWordCharacters);

        if (areEqual) {
            System.out.println( "Podane słowo jest anagramem");
        } else{
            System.out.println("Podane słowo nie jest anagramem");
        }

    }
}
