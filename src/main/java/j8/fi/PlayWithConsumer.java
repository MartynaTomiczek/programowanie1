package j8.fi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PlayWithConsumer {

    public static void main(String[] args) {

        Consumer <String> consumer =System.out::println;// nazwa interface funkcyjnego, <zmienne których bedzie używał>, nazwa
        Consumer <String> consumer1 =(String t) -> System.out.println(t);
        consumer.accept("Jakiś string");     // wywołanie metody .accept przywołyje stringa t

        List<String> stringList = new ArrayList<>(Arrays.asList("Kasia", "Basia", "Ania"));

        Consumer<List<String>> listConsumer =List::clear;
        Consumer<List<String>> listConsumer1=(List<String> t) -> t.clear();

        listConsumer.accept(stringList);

        System.out.println(stringList.size());

        MyConsumer<List<String>> addConsumer = (List<String> t) -> t.add("test");
        MyConsumer<List<String>> secondAddConsumer = (List<String> t) -> t.add("Hello");

        MyConsumer<List<String>> groupingConsumer = addConsumer.andThen(secondAddConsumer);

        groupingConsumer.accept(stringList);


    }


}
