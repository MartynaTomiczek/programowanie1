package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.io.File;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;


public class PlayWithStream {

    public static void main(String[] args) {
        List<Employee> employees=FileUtils.load(Path.of("C:/KURS/Programowanie1/Programowanie1/src/main/resources/j8/employees.csv"));


        //employees.stream()      // lista , strumień
        //   .filter(e-> e.getAge()>50)   // po liście filtr wiek większy niż 50 (e element)
        //   .collect(Collectors.toList());




        ex1(employees);
        ex2(employees);
        ex3(employees);
        ex4(employees);
        ex5(employees);
        ex6(employees);
        ex7(employees);
        ex8(employees);
       // ex9(employees);
        ex10(employees);
        ex11(employees);
        ex12(employees);
        ex13(employees);
        ex14(employees);
        ex14a(employees);
        ex14c(employees);
        ex14d(employees);
       //ex15(employees);




    }
    //Zadanie 15 Znajdz osobę najlepiej zarabiająca
   // private static void ex15(List<Employee> employees) {
    //  Optional<Employee> collect =   employees.stream()
        //        .collect(
            //            maxBy(
             //           Comparator.comparing(e->e.getSalary())
             //       )
      //          );

      //Optional<Employee> empGooo = employees
       //       .stream()
        //      .filter(e->e.getFirstName().equals(("GOOOO")))
         //     .findFirst();

     // boolean present = empGooo.isPresent();
    //  if (present) {
     //     Employee employee = empGooo.get();
     //     System.out.println(employee.getFirstName());
    //  } else{
   //       System.out.println("Nie ma go");

  //  }


    // Zadanie 14d. Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia
    //       jako Integer
    private static void ex14d(List<Employee> employees) {
    }

    // zadanie 14c Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia,
    //       zbierz imioną tylko te które kończą się na "a" - mapa której wartością bedą imiona
    private static void ex14c(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName(),
                                Collectors.filtering(
                                        e -> e.getFirstName().endsWith("a"),
                                        counting()
                                )
                        )
                );
        System.out.println(collect);
    }

    // zadanie 14a
    private static void ex14a(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(
                        groupingBy(
                                e->e.getFirstName(),
                                Collectors.counting()
                        )
                );
        System.out.println(collect);
    }



    // Zadanie 14 Stwórz mapę gdzie kluczem będzie Imię a wartościa List<Employee>
    private static void ex14(List<Employee> employees) {
        Map<Object, List<Employee>> collect = employees.stream()
                .collect(
                        groupingBy(
                                e->e.getFirstName()
                        )
                );
        System.out.println(collect);
    }

    // Zadanie 13 Wypisz średnią zarobków osób pracujących na pełny/pół etaptu
    private static void ex13(List<Employee> employees) {

        // wrzucamy do mapy : put.("Marcin" - to jest klucz, new Person("Marcin"))
        // put.("Szymon", new Person ("Szymon"))
        //
        // map.get ("Marcin") - wyciąganie z mapy

        Map<String, Double> collect = employees.stream()
                .collect(
                        groupingBy(
                                e-> e.getType().name(),
                                averagingDouble(
                                        e -> e.getSalary()
                                )
                        )

                );
        System.out.println(collect);
    }
    //zadanie 12  Wypisz ilość osób zarabiających wiecęj niż 5000, w formacie true->20, false->30 (patritinioningBy() + couting())

    private static void ex12(List<Employee> employees) {
        Map<Boolean, Long> collect=employees.stream()
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000,
                                Collectors.counting()

                                )
                        );

        System.out.println(collect);
    }

    // Zadanie 11 Wypisz wszystkie nazwiska w formacie {lastName,lastName}
    private static void ex11(List<Employee> employees) {
        String collect = employees.stream()
                .map (e -> e.getLastName())
                .collect(Collectors.joining(" | "));
        System.out.println(collect);;
        }

    //Zadanie 10 Posortuj listę osób w następujący sposób - wpierw nazwisko alfabetycznie rosnąco, następnie imię

    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(
                        Comparator.comparing(Employee::getLastName).thenComparing(Employee::getFirstName)    // comparator

                        // pierwotna wersja
                        //employees.stream()
                               // .sorted(
                                    //    (person1, person2) -> {
                                      //      int lastNameComparator=      // pierwszy comparator
                                      //              person1.getLastName()
                                       //                     .compareTo(person2.getLastName());
                                       //     if (lastNameComparator !=0) {
                                       //         return lastNameComparator;
                                       //     }
                                       //     return person1.getFirstName()
                                         //           .compareTo(person2.getFirstName());
                                      //  }
                );

    }

    // Zadanie 9 Wyfiltruj pracowników tak, aby pozostawić tych których drugi znak imienia to 'a'
    //     a czwarty nazwiska to 'b'
  // private static void ex9(List<Employee> employees) {
       // employees.stream()
           //     .filter(e-> e.getFirstName().charAt(2) == 'a')  // char jest pojedyńczym znakiem więc w pojedyńczym nawiasie a ie cudzysłowiu
             //   .filter(e -> e.getLastName().charAt(2) == 'b')   //pobieranie jednego znaku z łańcucha wtedy charAt
              //  .forEach(System.out::println);
   // }

    // Zadanie 8 Zasymuluj podwyżki pracowników o 12%, tworząc wynikową mapę której kluczem będzie nazwisko a wartościa wynagrodzenie po podwyżce
    private static void ex8(List<Employee> employees) {
        Map<String, List<Double>> collect=employees.stream()
                .collect(
                        groupingBy(
                                Employee::getLastName,
                                Collectors.mapping(
                                        employee -> employee.getSalary() * 0.12, toList()
                                )
                        )
                );
        System.out.println(collect);


    }


    //zadanie 7 Sprawdź czy w liscie instnieje osoba o nazwisku "Kowalski", jedną instrukcją
    private static void ex7(List<Employee> employees) {
        boolean kowalska = employees.stream()
                .anyMatch (e -> e.getLastName() .equals("Kowalska"));
        System.out.println(kowalska);

    }
    // zadanie 6 Wypisz profesje wszystkich pracownikow z dużych liter
    private static void ex6(List<Employee> employees) {
        employees.stream()
                .map(e-> e.getProfession() )  //.map(e-> e.getProffession().toUpperCase())
                .map(e -> e.toUpperCase())
                .forEach(System.out::println);


    }

    //zadanie 5  Wypisz 3 ostatnie litery z naziwska, jeśli kończa się na ski/ska maja być z dużych liter
    private static void ex5(List<Employee> employees) {
        employees.stream()
                .map(e->e.getLastName())
                .map(e -> e.substring(e.length()-3))     // przytnij długość nazwiska -3
                .map (e -> (e.endsWith("ski") || e.endsWith("ski"))    //warunki
                        ? e.toUpperCase() : e)
                .forEach(System.out::println);

    }


    // zadanie 4 imiona wszystkich pracowników

    private static void ex4(List<Employee> employees) {
        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);
    }

    // zad.3 osoby których nazwisko kończy się na ska i pracują na cały etat
    private static void ex3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ContractType.F))
                .forEach(System.out::println);
    }

    // zad.1 osoby których wynagorodzenie jest w przedziale 2500-3199

    private static void ex1(List<Employee> employees) {
        employees.stream()
                .filter(employee -> employee.getSalary() > 2500
                && employee.getSalary() < 3199)
                .forEach(System.out::println);

    }

    // zadanie 2

    private static void ex2(List<Employee> employees) {
        employees.stream()
                .filter(e->e.getAge() % 2 == 0)
                .forEach(System.out::println);
    }


}




