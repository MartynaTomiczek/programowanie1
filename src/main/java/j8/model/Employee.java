package j8.model;

public class Employee {
    String firstName;
    String lastName;
    int age;
    String profession;
    double salary;
    ContractType type;

    public Employee(String firstName, String lastName, int age, String profession, double salary, ContractType type) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.profession = profession;
        this.salary = salary;
        this.type = type;
    }

    public String getFirstName() {
        return firstName;
    }

    public Employee setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Employee setAge(int age) {
        this.age = age;
        return this;
    }

    public String getProfession() {
        return profession;
    }

    public Employee setProfession(String profession) {
        this.profession = profession;
        return this;
    }

    public double getSalary() {
        return salary;
    }

    public Employee setSalary(double salary) {
        this.salary = salary;
        return this;
    }

    public ContractType getType() {
        return type;
    }

    public Employee setType(ContractType type) {
        this.type = type;
        return this;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", profession='" + profession + '\'' +
                ", salary=" + salary +
                ", type=" + type +
                '}';
    }
}

