package j8.model;

public enum ContractType {
    F("FUL:"),
    H("HALF");

    String description;

    ContractType(String desc) {
        this.description = desc;
    }
    public String getDescription(){
        return description;
    }
}

