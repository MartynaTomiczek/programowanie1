package calculator;

public class Main {
    public static void main(String[] args) {
        Example.RED.something();
        Example.BLUE.something();
        Example.GREEN.something();

        System.out.println(Example.RED.getDesc());
        System.out.println(Example.BLUE.getDesc());
        System.out.println(Example.GREEN.getDesc());


        System.out.println(Calculator.ADD.calculate (4, 3));
        System.out.println(Calculator.SUBTRACT.calculate(9,4));
        System.out.println(Calculator.MULTIPLY.calculate(4,5));
    }
}
