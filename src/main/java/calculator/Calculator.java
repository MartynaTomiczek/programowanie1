package calculator;

import java.util.function.IntBinaryOperator;

public enum Calculator {

    ADD ("Dodawanie", (x,y) -> x + y),
    SUBTRACT("Odejmowanie" , (x,y) -> x - y),
    MULTIPLY("Mnożenie", (x,y) -> x * y);

    private String description;
    private IntBinaryOperator operator;

    Calculator (String description, IntBinaryOperator operator) {   // konstruktor
       this.description = description;
       this.operator = operator;
   }

   public int calculate (int a, int b){
        return operator.applyAsInt(a,b);
   }
}
