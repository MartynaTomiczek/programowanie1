package calculator;

public enum Example {

    BLUE ("NIE") {
        @Override
        public void something() {
            System.out.println("Something BLUE");
        }
    },
    GREEN ("ZIE"){
        @Override
        public void something() {
            System.out.println("Something GREEN");
        }
    },
    RED ("CZE") {
        @Override
        public void something() {
            System.out.println("Something RED");
        }
    };
    private String desc;

    Example(String desc) {
        this.desc = desc;
    }
    public String getDesc(){
        return desc;
    }

    public abstract void something();

}
