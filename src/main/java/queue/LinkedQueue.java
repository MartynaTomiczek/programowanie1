package queue;

public class LinkedQueue<T> implements Queue<T> {

    //Czoło naszej kolejki; tu przechowujemy nasze linki
    private Link<T> head;

    @Override
    //sprawdzamy czy mamy czoło kolejki
    // jeżeli nie to ustawić na nim nowy Link
    // jeżeli mamy czoło kolejki to w pętli przebiegamy po następnych ogniwach
    // czyli daj następnego nic nie zwraca

    // [LINK, LINK, LINK, ] <- offer (jak dodaj) 50 -> jak już bedzie pusto, po ostatnim elemencie wrzuci 50
    // [LINK -> daj następnego, LINK -> daj następnego, ....]
    public void offer(T element) {
        Link<T> newLink = new Link<>(element);
        if (head == null) {
            // jeżeli nie mamy czoła kolejki to je ustawiamy
            head = newLink;
        }else{
            // mamy czoło, przebiegamy w pętli
            //dopóki badane ogniwo ma kolejny element
            // idziemy do "ogona"
            Link<T> link = this.head;
            while (head.getNext() != null){
                //iterujemy aż dojdziemy do ogona (końca) czyli null
                link = link.getNext();
            }
            link.setNext(newLink);
        }

    }

    @Override
    public T poll() {
        //jeżeli kolejka jest pusta
        if (head == null){
            System.out.println("Empty queue");
            return null;
        }
        // jeżeli jest jest to pobieramy wartość head'a
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

    @Override
    public T peek() {
        return (T) head.getValue();
    }

    @Override
    public int size() {
        return 0;
    }
}
