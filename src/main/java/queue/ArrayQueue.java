package queue;

import java.util.Arrays;

public class ArrayQueue <T> implements Queue <T>{

    private Object[] elements;

    public ArrayQueue() {
        this.elements = new Object[0];
    }

    //[1] 2 ,3
    // do kolejnych elementów kopia tablicy gdzie jej wielkość będzie zawsze size + 1
    //[]
    //[]  <- 1
    // [] -> copyOf(stara tablica, rozmiar starej +1)
    //[ ,] -> elements[elements.lenght -1] -> 145
    // [145]
    //[145] <- nowe 200
    //Arrays.copyOf ([145], [145, ]
    //elements[elements.lenght - 1] -> [145,200]
    // Arrays.copyOf()
    // elements[elements.lenght - 1] = element;



    @Override
    public void offer(T element) {
        // tworzenie kopi tablicy aby zwiększyć jej rozmiar
        elements = Arrays.copyOf(
                elements, elements.length + 1);
        // dodatemy element na 1 miejscu kolejki
        elements[elements.length -1] = element;
    }

    @Override
    // ściąga pierwszy element zwraca i usuwa z kolejki
    //[12,45,20] <- poll()
    //12 - dostaje 1 element
    //
    //

    public T poll() {
        if (elements.length== 0) {
            System.out.println("Queue is empty");
            return null;
        }
        //pobranie pierwszego elementu z kolejki
       T element = (T) elements[0];
        elements = Arrays.copyOfRange
                (elements,1, elements.length);  //od elementu 2 do długości tablicy
        return element;
    }

    @Override
    public T peek() {
        if (elements.length== 0) {
            System.out.println("Queue is empty");
            return null;
        }
        return (T) elements[0];

    }

    @Override
    public int size() {
        return elements.length;

    }
}
