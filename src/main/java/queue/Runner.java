package queue;

import java.lang.reflect.Array;

public class Runner {
    public static void main(String[] args) {
        ArrayQueue<Integer>
                queue = new ArrayQueue<>();
        queue.offer(2);
        queue.offer(5);
        queue.offer(9);
        System.out.println(queue.poll());
        System.out.println(queue.peek());
    }
}
