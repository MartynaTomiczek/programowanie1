package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {
    public static void main(String[] args) throws IOException {
        // (masa / 3 -> wynik zaokrąglić) - 2 ---> PALIWO
        // dla każdego wpisu liczymy paliwo
        // następnie sumujemy wszystko

        // 1. wczytanie pliku i wypisanie wartośi na konsole
        //2. metoda która będzie nam liczyła ilość paliwa (poza mainem)
        // 3. pętla obliczjąca potrzebne paliwo dla każdej wartości
        // 4. żeby zsumować napisz zmiennną pomocniczą sum
        //DRUGA CZESC
        //5. dla każdej sumy wynik /3, -2 i tak, aż wynik będzie mniejszy od zera
        // 6. zmiana metody obliczeniowej


       Stream<String> inputValues =                                                             //zaczytanie pliku
        Files.lines(Path.of("src/main/resources/adventofcode/day1input"));
        List<String> ListOfModuleMass = inputValues.collect(Collectors.toList());

        int sum = 0;
        for (String mass:ListOfModuleMass) {
            int requiredFuel = calculateFuel(Integer.parseInt(mass));
            sum += requiredFuel;
            System.out.println("Fuel needed for mass " + mass+ " ->>> " + requiredFuel);
        }
        System.out.println("Sum of required fuel: "+ sum);


    }

    // rekurencja (zmiana metody wyliczeniowej na metode-rekurencje)

    public static int calculateFuel (int mass) {
        int fuel = (mass/3)-2;
        if (fuel <= 0){
            return 0;
            } else {
            return fuel + calculateFuel(fuel);
        }
    }

}
